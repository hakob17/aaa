package am.taxi.web.resource;


import am.taxi.domain.User;
import am.taxi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HealthResource {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("health")
    public String health() {
        return "OK";
    }

    @GetMapping("create")
    public User createUser() {

        User u = new User("+37455119561");
        userRepository.save(u);

        return u;
    }

    @GetMapping("all")
    public List<User> allUsers() {

        return userRepository.findAll();
    }
}
