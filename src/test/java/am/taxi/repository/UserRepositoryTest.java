package am.taxi.repository;


import am.taxi.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.Date;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Before
    public void before() {

    }

    @Test
    public void getAllUsers() {
        User u = new User();
        u.setPhoneNumber("+37455119561");
        u.setFirstName("Hakob");
        u.setLastName("Hakobyan");
        u.setBirthDay(new Date());

        userRepository.save(u);

        Assert.notEmpty(userRepository.findAll());

    }
}
